## Ansible Docker

This role is intented for [docker (community edition)](https://www.docker.com/) 
installation on your Ubuntu server(s).
It implements [this](https://docs.docker.com/install/linux/docker-ce/ubuntu) 
article.


### Usage
As usual, you have to have ssh access to required servers.

Example inventory (yaml format):
```yaml
docker:
  hosts:
    ip1:
    ip2:
    ip3:
  
  vars:
      ansible_user: root
      ansible_ssh_private_key_file: /path/to/private/id_rsa
```

Usage example:
```bash
ansible-playbook -i inventory.yml playbook.yml -vv
```
